package com.azawisza.highscore;

import com.azawisza.highscore.common.GameServerConfiguration;
import com.azawisza.highscore.common.GameServerContext;

import com.azawisza.highscore.common.ServerConfigurationBuilder;
import com.azawisza.highscore.common.server.GameServer;
import org.apache.log4j.Logger;


/**
 * Created by azawisza on 15.10.2016.
 */
public class GameRunner {

    private final static Logger logger = Logger.getLogger(GameRunner.class.getName());

    public static void main(String[] args) {
        System.out.println("Supported comand line arguments: port, corePoolSize, maximumPoolSize, maxWaitingRq, keepAliveTimeSec, loggingEnabled, for example: loggingEnabled=true port=8091");
        GameServerConfiguration configuration = new ServerConfigurationBuilder()
                .buildConfigurationFromArguments(args);
        logger.debug(configuration);
        GameServerContext context = new GameServerContext();
        GameServer server = new GameServer(configuration,
                context);
        server.start();
        Runtime.getRuntime().addShutdownHook(new Thread(()->server.stop()));
    }


}
