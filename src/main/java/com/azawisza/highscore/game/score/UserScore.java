package com.azawisza.highscore.game.score;

/**
 * Created by azawisza on 18.10.2016.
 */
public class UserScore {

    private final int userId;

    private int score;

    public UserScore(int userId, int score) {
        this.userId = userId;
        this.score = score;
    }

    public int getUserId() {
        return userId;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserScore)) return false;
        UserScore userScore = (UserScore) o;
        if (userId != userScore.userId) return false;
        return score == userScore.score;

    }

    @Override
    public int hashCode() {
        int result = userId;
        result = 31 * result + score;
        return result;
    }
}
