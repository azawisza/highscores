package com.azawisza.highscore.common.service;

/**
 * Created by azawisza on 16.10.2016.
 */
public class ServiceResult {

    private int code;
    private String result;

    public ServiceResult(int code, String result) {
        this.code = code;
        this.result = result;
    }

    public int getCode() {
        return code;
    }

    public String getResult() {
        return result;
    }

    @Override
    public String toString() {
        return "ServiceResult{" +
                "code=" + code +
                ", result='" + result + '\'' +
                '}';
    }
}
