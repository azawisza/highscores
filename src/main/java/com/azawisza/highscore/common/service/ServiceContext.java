package com.azawisza.highscore.common.service;

import java.util.List;

/**
 * Created by azawisza on 16.10.2016.
 */
public interface ServiceContext {

    public boolean supports(ServiceRequest request);

    public List<DeclarableService> methods();
}
