package com.azawisza.highscore.game.score;

import com.azawisza.highscore.common.service.ServiceRequest;
import com.azawisza.highscore.common.service.ServiceRequestConverter;
import com.azawisza.highscore.game.GameException;

import java.util.Optional;

import static com.azawisza.highscore.game.GameConsts.BadRequest400;

/**
 * Created by azawisza on 17.10.2016.
 */
public class ScoreRequestConverter implements ServiceRequestConverter<ScoreRequest> {

    public static final int SCORE_INDEX = 1;

    @Override
    public ScoreRequest convert(ServiceRequest request) {
        Integer score = Integer.valueOf(request.getBody());
        Integer levelId = Integer.valueOf(request.getPathElement(SCORE_INDEX));

        Optional<String> sessionkey = request.getQueryValueOf("sessionkey");
        if (!sessionkey.isPresent()) {
            throw new GameException("No sessionkey in score request", BadRequest400);
        }
        return new ScoreRequest()
                .withScore(score)
                .withLevelId(levelId)
                .withSessionId(sessionkey.get());
    }
}
