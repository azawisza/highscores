package com.azawisza.highscore.common.service;

/**
 * Created by azawisza on 16.10.2016.
 */
@FunctionalInterface
public interface Service<RQ> {

    ServiceResult service(RQ request);

}
