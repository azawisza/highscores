package com.azawisza.highscore.common.service;


import com.azawisza.highscore.common.HttpCodes;

/**
 * Created by azawisza on 25.10.2016.
 */
public class ServiceResponse {

    private int code;
    private String result;

    public static ServiceResponse ok200With(Object result) {
        return new ServiceResponse(HttpCodes.OK200, result.toString());
    }

    public static ServiceResponse notFound404() {
        return new ServiceResponse(HttpCodes.NotFound, "");
    }

    public ServiceResponse(int code, String result) {
        this.code = code;
        this.result = result;
    }

    public int getCode() {
        return code;
    }

    public String getResult() {
        return result;
    }

    @Override
    public String toString() {
        return "ServiceResponse{" +
                "code=" + code +
                ", result='" + result + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ServiceResponse)) return false;

        ServiceResponse that = (ServiceResponse) o;

        if (code != that.code) return false;
        return result != null ? result.equals(that.result) : that.result == null;

    }

    @Override
    public int hashCode() {
        int result1 = code;
        result1 = 31 * result1 + (result != null ? result.hashCode() : 0);
        return result1;
    }
}
