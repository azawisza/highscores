package com.azawisza.highscore.game.login;

import com.azawisza.highscore.common.service.Service;
import com.azawisza.highscore.common.service.ServiceResult;
import com.azawisza.highscore.game.GameConfiguration;
import com.azawisza.highscore.game.repositories.UserSessionRepository;

import java.util.UUID;
import java.util.function.BiFunction;

import static com.azawisza.highscore.game.GameConsts.OK200;

/**
 * Created by azawisza on 16.10.2016.
 */
public class UserLoginService implements Service<Integer> {

    private final UserSessionRepository userSessionRepository;
    private final GameConfiguration conf;

    public UserLoginService(UserSessionRepository userSessionRepository, GameConfiguration conf) {
        this.userSessionRepository = userSessionRepository;
        this.conf = conf;
    }

    @Override
    public ServiceResult service(Integer userid) {
        UserSession possibleNewSession = createSession();
        String sessionId = userSessionRepository.upsertAndLoginUser(userid, possibleNewSession, invalidateSession());
        return new ServiceResult(OK200, sessionId);
    }

    private UserSession createSession() {
        Long expiry = System.currentTimeMillis() + conf.getSessionLength();
        UUID uuid = UUID.randomUUID();
        return new UserSession(expiry, "s" + uuid.toString());
    }

    private BiFunction<UserSession, UserSession, UserSession> invalidateSession() {
        return (currentSession, newSessionCandidate) -> {
            if (newSessionCandidate.getExpiry() - conf.getSessionLength() > currentSession.getExpiry()) {
                return newSessionCandidate;
            } else {
                return currentSession;
            }
        };
    }

}
