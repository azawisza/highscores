package com.azawisza.highscore.common.server;

import com.azawisza.highscore.common.service.*;
import com.azawisza.highscore.game.GameException;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.apache.log4j.Logger;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.azawisza.highscore.common.server.ServerUtils.send;
import static com.azawisza.highscore.game.GameConsts.BadRequest400;
import static com.azawisza.highscore.game.GameConsts.ServerError500;

/**
 * Created by azawisza on 16.10.2016.
 */
public class DefaultHandler implements HttpHandler {

    public static final String encoding_ISO_8859_1 = "ISO-8859-1";
    public static final int BUFFER_SIZE = 4096;
    public static final String PATH_SEPARATOR = "/";
    private final ServiceContext serviceContext;

    private final static Logger logger = Logger.getLogger(DefaultHandler.class.getName());

    private DefaultHandler(ServiceContext serviceContext) {
        this.serviceContext = serviceContext;
    }

    public static HttpHandler handle(ServiceContext serviceContext) {
        return new DefaultHandler(serviceContext);
    }

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        try {
            ServiceRequest serviceRequest = getServiceRequest(httpExchange);
            logger.info(serviceRequest.toString());
            if (serviceContext.supports(serviceRequest)) {
                for (DeclarableService method : serviceContext.methods()) {
                    if (method.matches(serviceRequest)) {
                        invokeServiceMethod(httpExchange, serviceRequest, method);
                        return;
                    }
                }
            } else {
                send(httpExchange, "This call is not supported", BadRequest400);
                return;
            }
        } catch (GameException e3) {
            send(httpExchange, "Server Error", ServerError500);
        } catch (NumberFormatException e2) {
            send(httpExchange, "Invalid number in request", BadRequest400);
        }
        send(httpExchange, "This call is not supported", BadRequest400);
    }

    private void invokeServiceMethod(HttpExchange httpExchange, ServiceRequest serviceRequest, DeclarableService method) {
        Service service = method.get();
        ServiceRequestConverter requestConverter = method.getConverter();
        Object convertedRequest = requestConverter.convert(serviceRequest);
        ServiceResult result  = service.service(convertedRequest);
        logger.info(result.toString());
        send(httpExchange, result.getResult(), result.getCode());
    }

    private ServiceRequest getServiceRequest(HttpExchange arg) {
        String requestMethod = arg.getRequestMethod();
        URI requestURI = arg.getRequestURI();
        String pathString = requestURI.getPath();
        List<String> pathAsList = Arrays.asList(pathString.split(PATH_SEPARATOR));
        List<String> queries = getQueries(requestURI.getQuery());
        return new ServiceRequest(requestMethod, pathAsList, convertRequestBodyToText(arg), queries);
    }

    private List<String> getQueries(String query) {
        List<String> queries = new ArrayList<>();
        if (query != null && !query.isEmpty()) {
            queries.addAll(Arrays.asList(query.split("&")));
        }
        return queries;
    }

    private String convertRequestBodyToText(HttpExchange arg) {
        ByteArrayOutputStream out;
        String bodyAsText = "";
        try (InputStream requestBody = arg.getRequestBody()) {
            out = new ByteArrayOutputStream();
            byte buffer[] = new byte[BUFFER_SIZE];
            for (int n = requestBody.read(buffer); n > 0; n = requestBody.read(buffer)) {
                out.write(buffer, 0, n);
            }
            bodyAsText = new String(out.toByteArray(), encoding_ISO_8859_1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bodyAsText;
    }

}
