package com.azawisza.highscore.common.service;

import java.util.List;
import java.util.Optional;

/**
 * Created by azawisza on 16.10.2016.
 */
public class ServiceRequest {

    public static final String QUERY_KEY_VALUE_SEPARATOR = "=";
    private String method;
    private List<String> path;
    private String body;
    private final List<String> query;


    public ServiceRequest(String method, List<String> path, String body, List<String> query) {
        this.method = method;
        this.path = path;
        this.body = body;
        this.query = query;
    }

    public String getBody() {
        return body;
    }

    public String getMethod() {
        return method;
    }

    public String getPathElement(int index) {
        return path.get(index);
    }

    public int getQuerySize() {
        return query.size();
    }

    public Optional<String> getQueryValueOf(String key) {
        Optional<String> first = query.stream().filter((v) -> extractQueryName(v).equals(key)).findFirst();
        String value = null;
        if (first.isPresent()) {
            String keyValue = first.get();
            value = keyValue.substring(keyValue.indexOf(QUERY_KEY_VALUE_SEPARATOR) + 1, keyValue.length());
        }
        return Optional.ofNullable(value);
    }

    private String extractQueryName(String queryKeyValuePair) {
        String result = "";
        if (queryKeyValuePair != null && queryKeyValuePair.contains("=")) {
            result = queryKeyValuePair.substring(0, queryKeyValuePair.indexOf("="));
        }
        return result;
    }

    public int pathSize() {
        return path.size();
    }

    @Override
    public String toString() {
        return "ServiceRequest{" +
                "method='" + method + '\'' +
                ", path=" + path +
                ", body='" + body + '\'' +
                ", query=" + query +
                '}';
    }
}
