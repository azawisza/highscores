package com.azawisza.highscore.game.score;

/**
 * Created by azawisza on 17.10.2016.
 */
public class ScoreRequest {

    private int levelId;
    private String sessionId;
    private int score;


    public int getLevelId() {
        return levelId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public int getScore() {
        return score;
    }

    public ScoreRequest withLevelId(final int levelId) {
        this.levelId = levelId;
        return this;
    }

    public ScoreRequest withSessionId(final String sessionId) {
        this.sessionId = sessionId;
        return this;
    }

    public ScoreRequest withScore(final int score) {
        this.score = score;
        return this;
    }

}
