package com.azawisza.highscore.game.login;

import com.azawisza.highscore.common.service.ServiceRequest;
import org.fest.assertions.Assertions;
import org.junit.Test;

import static java.util.Arrays.asList;


/**
 * Created by azawisza on 17.10.2016.
 */
public class LoginRequestConverterTest {

    @Test
    public void shouldConvert() {
        //given
        LoginRequestConverter converter = new LoginRequestConverter();
        //when
        Integer request = converter.convert(
                new ServiceRequest("GET", asList("","112", "login"), "", asList()));
        //then
        Assertions.assertThat(request).isEqualTo(112);
    }

}