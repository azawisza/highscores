package com.azawisza.highscore.common.server;

import com.azawisza.highscore.common.GameServerConfiguration;
import com.azawisza.highscore.common.GameServerContext;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by azawisza on 15.10.2016.
 */
public class GameServer {

    private final GameServerConfiguration conf;
    private GameServerContext context;
    private HttpServer httpServer;
    private ExecutorService executorService;
    private final static Logger logger = Logger.getLogger(GameServer.class.getName());

    public GameServer(GameServerConfiguration conf, GameServerContext context) {
        this.conf = conf;
        this.context = context;
    }

    public void start() {
        httpServer = createServer();
        executorService = createThreadPool();
        httpServer.setExecutor(executorService);
        setupContext(httpServer);
        httpServer.start();
    }

    public void stop() {
        logger.info("Shutting down server...");
        httpServer.stop(1);
        logger.info("Shutting down timer");
        executorService.shutdown();
        try {
            if (!executorService.awaitTermination(2, TimeUnit.SECONDS)) {
                logger.warn("Shutting down timer abruptly");
                executorService.shutdownNow();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        logger.info("Shutting down timer - done");
    }

    private void setupContext(HttpServer httpServer) {
        for (Map.Entry<String, HttpHandler> entry : context) {
            httpServer.createContext(entry.getKey(), entry.getValue());
        }
    }

    private ExecutorService createThreadPool() {
        LinkedBlockingQueue<Runnable> workQueue = new LinkedBlockingQueue<>(conf.getMaxWaitingRq());
        ExecutorService service = new ThreadPoolExecutor(
                conf.getCorePoolSize(),
                conf.getMaximumPoolSize(),
                conf.getKeepAliveTime(),
                conf.getKeepAliveUnit(),
                workQueue);
        return service;
    }

    private HttpServer createServer() {
        HttpServer httpServer = null;
        try {
            httpServer = HttpServer.create(new InetSocketAddress(conf.getPort()), 0);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return httpServer;
    }
}
