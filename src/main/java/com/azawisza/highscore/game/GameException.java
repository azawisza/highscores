package com.azawisza.highscore.game;

/**
 * Created by azawisza on 17.10.2016.
 */
public class GameException extends RuntimeException {
    private final int code;

    public GameException(String s, int code) {
        super(s);
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
