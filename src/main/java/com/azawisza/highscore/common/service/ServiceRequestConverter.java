package com.azawisza.highscore.common.service;

/**
 * Created by azawisza on 17.10.2016.
 */
@FunctionalInterface
public interface ServiceRequestConverter<T> {
    T convert(ServiceRequest request);

}
