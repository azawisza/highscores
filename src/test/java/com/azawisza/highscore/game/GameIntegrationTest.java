package com.azawisza.highscore.game;

import com.azawisza.highscore.common.GameServerConfiguration;
import com.azawisza.highscore.common.GameServerContext;
import com.azawisza.highscore.common.server.GameServer;
import org.fest.assertions.Assertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by azawisza on 17.10.2016.
 */
public class GameIntegrationTest {

    public static final int TEST_SERVER_PORT = 8089;
    public static final String URL = "http://localhost:" + TEST_SERVER_PORT;
    private GameServer server;

    @Before
    public void setUp() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        GameServerContext context = new GameServerContext();
        server = new GameServer(new GameServerConfiguration()
                .withPort(TEST_SERVER_PORT)
                .withCorePoolSize(1)
                .withMaximumPoolSize(Integer.MAX_VALUE)
                .withKeepAliveTime(60L)
                .withKeepAliveUnit(TimeUnit.SECONDS)
                .withMaxWaitingRq(10),
                context);
        Thread thread = new Thread(() -> {
            server.start();
            latch.countDown();
        });
        thread.start();
        latch.await();
    }

    @Test
    public void shouldGetSessionAndAddUser() throws IOException, InterruptedException {
        //given
        //when
        String response = callAndGetServerResponse("http://localhost:" + TEST_SERVER_PORT + "/321/login", null);
        //then
        assertThat(response).isNotEmpty();
        assertThat(response).contains("s");
    }

    @Test(expected = IOException.class,timeout = 5000)
    public void shouldResponseWhenCallNotSupported() throws IOException, InterruptedException {
        //given
        //when
        String response = callAndGetServerResponse("http://localhost:" + TEST_SERVER_PORT + "/321/notsuported", null);
        //then
    }

    @Test(expected = IOException.class,timeout = 5000)
    public void shouldResponseWhenCallNotSupportedBecauseOfLengthOfPath() throws IOException, InterruptedException {
        //given
        //when
        String response = callAndGetServerResponse("http://localhost:" + TEST_SERVER_PORT + "/321/notsuported/notsuported", null);
        //then
    }

    @Test(expected = IOException.class,timeout = 5000)
    public void shouldResponseWhenCallNotSupportedBecauseOfMethod() throws IOException, InterruptedException {
        //given
        String session = callAndGetServerResponse(URL + "/321/login", null);
        callAndGetServerResponse(URL + "/1111/score?sessionkey=" + session, "1100");
        //when
        String response = callAndGetServerResponse(URL + "/1111/score?sessionkey=" + session, null);
        //then
    }

    @Test
    public void shouldAddScoreAndDisplayResults() throws IOException, InterruptedException {
        //given
        String session = callAndGetServerResponse(URL + "/321/login", null);
        callAndGetServerResponse(URL + "/1111/score?sessionkey=" + session, "1100");
        String highScores = callAndGetServerResponse("http://localhost:" + TEST_SERVER_PORT + "/1111/highscorelist", null);
        //when
        //then
        assertThat(session).contains("s");
        assertThat(highScores).isEqualTo("321=1100");
    }

    private String callAndGetServerResponse(String spec, String body) throws IOException {

        URL url = new URL(spec);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        if (body != null) {
            byte[] postData = body.getBytes(Charset.defaultCharset());
            int postDataLength = postData.length;
            conn.setInstanceFollowRedirects(false);
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty("charset", "utf-8");
            conn.setUseCaches(false);
            conn.setRequestProperty("Content-Length", Integer.toString(postDataLength));
            conn.setRequestMethod("POST");
            try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
                wr.write(postData);
            }
        }
        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String s = in.readLine();
        in.close();
        return s;
    }


    @After
    public void tearDown() {
        server.stop();
    }

}
