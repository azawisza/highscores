package com.azawisza.highscore.game.score;

import com.azawisza.highscore.common.service.ServiceResult;
import com.azawisza.highscore.game.ServiceTestBase;
import org.fest.assertions.Assertions;
import org.junit.Test;

/**
 * Created by azawisza on 18.10.2016.
 */
public class ScoreListTest extends ServiceTestBase {

    @Test
    public void shouldReturnSingleResult() {
        //given
        int levelId = 21;
        String sessionId = userLoginService.service(1233).getResult();
        callWithScore(levelId, sessionId, 12);
        //when
        ServiceResult service = scoreListService.service(levelId);
        //then
        Assertions.assertThat(service.getResult()).isEqualTo("1233=12");
    }
    @Test
    public void shouldReturnMultipleResultsOfDifferentUsers() {
        //given
        int levelId = 21;
        String sessionId = userLoginService.service(1233).getResult();
        String sessionId2 = userLoginService.service(1244).getResult();
        String sessionId3 = userLoginService.service(1255).getResult();
        callWithScore(levelId, sessionId, 12);
        callWithScore(levelId, sessionId2, 25);
        callWithScore(levelId, sessionId3, 56);
        //when
        ServiceResult service = scoreListService.service(levelId);
        //then
        Assertions.assertThat(service.getResult()).isEqualTo("1255=56,1244=25,1233=12");
    }


}
