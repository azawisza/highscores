package com.azawisza.highscore.game.login;

import com.azawisza.highscore.common.service.ServiceResult;
import com.azawisza.highscore.game.ServiceTestBase;
import org.fest.assertions.Assertions;
import org.junit.Test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by azawisza on 18.10.2016.
 */
public class UserLoginServiceTest extends ServiceTestBase {

    @Test
    public void shouldAddNewUserAndStartSession() {
        //given
        ServiceResult service = userLoginService.service(3213);
        //when
        String result = service.getResult();
        //then
        assertThat(result).isNotNull();
        assertThat(result).contains("s");
        assertThat(service.getCode()).isEqualTo(200);
    }

    @Test
    public void shouldReturnCurrentSession() {
        //given
        ServiceResult firstSession = userLoginService.service(3213);
        //when
        ServiceResult secondCallResult = userLoginService.service(3213);
        Integer userOfSessionId = userSessionRepository.findUserOfSessionId(secondCallResult.getResult());
        //then
        assertThat(secondCallResult.getResult()).isEqualTo(firstSession.getResult());
        assertThat(secondCallResult.getCode()).isEqualTo(200);
        assertThat(userOfSessionId).isEqualTo(3213);
    }

    @Test
    public void shouldInvalidateSession() throws InterruptedException {
        //given
        configuration.withSessionLength(1000);
        ServiceResult oldSession = userLoginService.service(3213);
        CountDownLatch latch = new CountDownLatch(1);
        new Thread(sessionInvalidationCheck(oldSession.getResult(), latch))
                .start();
        //when
        latch.await(10, TimeUnit.SECONDS);
        ServiceResult newSessionForSameUser = userLoginService.service(3213);
        Integer userOfSessionId = userSessionRepository.findUserOfSessionId(newSessionForSameUser.getResult());
        //then
        Assertions.assertThat(userOfSessionId).isEqualTo(3213);
        Assertions.assertThat(newSessionForSameUser.getResult()).isNotEqualTo(oldSession.getResult());
    }




}