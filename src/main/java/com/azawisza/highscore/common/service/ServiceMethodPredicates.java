package com.azawisza.highscore.common.service;

import java.util.function.BiPredicate;
import java.util.function.Predicate;

/**
 * Created by azawisza on 16.10.2016.
 */
public class ServiceMethodPredicates {

    public static final int METHOD_NAME_INDEX = 2;
    public static final Predicate<ServiceRequest> hasQuery = (path) -> path.getQuerySize() > 0;
    public static final Predicate<ServiceRequest> pathOfThree = (path) -> path.pathSize() == 3;
    public static final Predicate<ServiceRequest> methodGet = (path) -> path.getMethod().equals("GET");
    public static final Predicate<ServiceRequest> methodPost = (path) -> path.getMethod().equals("POST");
    public static final BiPredicate<ServiceRequest, String> secondPathElementIs = (path, name) -> path.getPathElement(METHOD_NAME_INDEX).equals(name);

}
