package com.azawisza.highscore.game.highscore;

import com.azawisza.highscore.common.service.ServiceRequest;
import org.fest.assertions.Assertions;
import org.junit.Test;

import static java.util.Arrays.asList;


/**
 * Created by azawisza on 17.10.2016.
 */
public class HighScoreRequestConverterTest {

    @Test
    public void shouldConvert() {
        //given
        HighScoreRequestConverter converter = new HighScoreRequestConverter();
        //when
        Integer request = converter.convert(
                new ServiceRequest("GET", asList("","112", "highscorelist"), "", asList()));
        //then
        Assertions.assertThat(request).isEqualTo(112);
    }

}