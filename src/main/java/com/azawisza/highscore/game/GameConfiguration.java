package com.azawisza.highscore.game;

/**
 * Created by azawisza on 18.10.2016.
 */
public class GameConfiguration {

    private long sessionLength;
    private long threshold;
    private int maxLevelResults;

    public long getSessionLength() {
        return sessionLength;
    }

    public long getThreshold() {
        return threshold;
    }

    public GameConfiguration withSessionLength(final long sessionLength) {
        this.sessionLength = sessionLength;
        return this;
    }

    public GameConfiguration withThreshold(final long treshold) {
        this.threshold = treshold;
        return this;
    }


    public void setMaxLevelResults(int maxLevelResults) {
        this.maxLevelResults = maxLevelResults;
    }

    public int getMaxLevelResults() {
        return maxLevelResults;
    }

    public int maxLevelResults() {
        return this.maxLevelResults;
    }

    public GameConfiguration withMaxLevelResults(final int maxLevelResults) {
        this.maxLevelResults = maxLevelResults;
        return this;
    }


}
