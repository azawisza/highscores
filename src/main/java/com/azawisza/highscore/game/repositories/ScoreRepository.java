package com.azawisza.highscore.game.repositories;

import com.azawisza.highscore.game.GameConfiguration;
import com.azawisza.highscore.game.score.UserScore;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.BiFunction;

import static java.util.stream.Collectors.toCollection;

/**
 * Created by azawisza on 18.10.2016.
 */
public class ScoreRepository {

    private final GameConfiguration conf;
    private ConcurrentHashMap<Integer, CopyOnWriteArrayList<UserScore>> map = new ConcurrentHashMap<>();

    public ScoreRepository(GameConfiguration configuration) {
        this.conf = configuration;
    }

    public void addUserScoreToLevel(int levelId, int userId, int score) {
        CopyOnWriteArrayList<UserScore> possiblyNewValue = new CopyOnWriteArrayList<>();
        UserScore newScoreEntryCandidate = new UserScore(userId, score);
        possiblyNewValue.add(newScoreEntryCandidate);
        map.merge(levelId, possiblyNewValue,
                mergeNewWithCurrent());
    }

    public List<UserScore> getHighScoreOfLevel(int levelId) {
        CopyOnWriteArrayList<UserScore> userScores = map.get(levelId);
        return userScores != null ? userScores : new ArrayList<>();
    }

    private BiFunction<CopyOnWriteArrayList<UserScore>, CopyOnWriteArrayList<UserScore>, CopyOnWriteArrayList<UserScore>> mergeNewWithCurrent() {
        return (currentScoreList, possiblyNewValue) -> {
            UserScore possiblyNewScore = possiblyNewValue.iterator().next();
            updateScore(currentScoreList, possiblyNewScore);
            currentScoreList = currentScoreList.stream()
                    .sorted(Comparator.comparing(UserScore::getScore).reversed())
                    .limit(conf.getMaxLevelResults())
                    .collect(toCollection(CopyOnWriteArrayList::new));
            return currentScoreList;
        };
    }

    private void updateScore(CopyOnWriteArrayList<UserScore> currentScoreList, UserScore possiblyNewScore) {
        for (UserScore score : currentScoreList) {
            if (score.getUserId() == possiblyNewScore.getUserId()) {
                if (score.getScore() < possiblyNewScore.getScore()) {
                    score.setScore(possiblyNewScore.getScore());
                }
                return;
            }
        }
        currentScoreList.add(possiblyNewScore);
    }


}
