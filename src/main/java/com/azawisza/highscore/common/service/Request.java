package com.azawisza.highscore.common.service;

import java.util.Optional;
import java.util.function.Predicate;

/**
 * Created by azawisza on 01.11.2016.
 */
public interface Request {

    Optional<Predicate<ServiceRequest>> condition();
}
