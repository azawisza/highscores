package com.azawisza.highscore.common;

import java.util.concurrent.TimeUnit;

/**
 * Created by azawisza on 15.10.2016.
 */
public class GameServerConfiguration {

    private int port;
    private int corePoolSize;
    private int maximumPoolSize;
    private long keepAliveTime;
    private TimeUnit keepAliveUnit;
    private int maxWaitingRq;
    private boolean loggingEnabled;

    public GameServerConfiguration withPort(final int port) {
        this.port = port;
        return this;
    }

    public GameServerConfiguration withCorePoolSize(final int corePoolSize) {
        this.corePoolSize = corePoolSize;
        return this;
    }

    public GameServerConfiguration withMaximumPoolSize(final int maximumPoolSize) {
        this.maximumPoolSize = maximumPoolSize;
        return this;
    }

    public GameServerConfiguration withKeepAliveTime(final long keepAliveTime) {
        this.keepAliveTime = keepAliveTime;
        return this;
    }

    public GameServerConfiguration withKeepAliveUnit(final TimeUnit keepAliveUnit) {
        this.keepAliveUnit = keepAliveUnit;
        return this;
    }

    public GameServerConfiguration withMaxWaitingRq(final int maxWaitingRq) {
        this.maxWaitingRq = maxWaitingRq;
        return this;
    }

    public GameServerConfiguration withLoggingEnabled(final boolean loggingEnabled) {
        this.loggingEnabled = loggingEnabled;
        return this;
    }

    public int getPort() {
        return port;
    }

    public int getCorePoolSize() {
        return corePoolSize;
    }

    public int getMaximumPoolSize() {
        return maximumPoolSize;
    }

    public long getKeepAliveTime() {
        return keepAliveTime;
    }

    public TimeUnit getKeepAliveUnit() {
        return keepAliveUnit;
    }

    public int getMaxWaitingRq() {
        return maxWaitingRq;
    }

    public boolean isLoggingEnabled() {
        return loggingEnabled;
    }

    @Override
    public String toString() {
        return "GameServerConfiguration{" +
                "port=" + port +
                ", corePoolSize=" + corePoolSize +
                ", maximumPoolSize=" + maximumPoolSize +
                ", keepAliveTimeForIdleThread=" + keepAliveTime +
                ", keepAliveUnit=" + keepAliveUnit +
                ", maxWaitingRq=" + maxWaitingRq +
                ", loggingEnabled=" + loggingEnabled +
                '}';
    }
}
