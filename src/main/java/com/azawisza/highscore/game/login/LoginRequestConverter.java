package com.azawisza.highscore.game.login;

import com.azawisza.highscore.common.service.ServiceRequest;
import com.azawisza.highscore.common.service.ServiceRequestConverter;

/**
 * Created by azawisza on 17.10.2016.
 */
public class LoginRequestConverter implements ServiceRequestConverter<Integer> {

    public static final int LOGIN_INDEX = 1;

    @Override
    public Integer convert(ServiceRequest request) {
        return getUserId(request);
    }

    private Integer getUserId(ServiceRequest request) {
        String pathElement = request.getPathElement(LOGIN_INDEX);
        return Integer.valueOf(pathElement);
    }

}
