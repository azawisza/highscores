package com.azawisza.highscore.game.highscore;


import com.azawisza.highscore.common.service.ServiceRequest;
import com.azawisza.highscore.common.service.ServiceRequestConverter;

/**
 * Created by azawisza on 17.10.2016.
 */
public class HighScoreRequestConverter implements ServiceRequestConverter<Integer> {

    public static final int SOCORE_INDDEX = 1;

    @Override
    public Integer convert(ServiceRequest request) {
        String pathElement = request.getPathElement(SOCORE_INDDEX);
        return Integer.valueOf(pathElement);
    }
}
