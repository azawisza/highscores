package com.azawisza.highscore.game.highscore;

import com.azawisza.highscore.common.service.Service;
import com.azawisza.highscore.common.service.ServiceResult;
import com.azawisza.highscore.game.repositories.ScoreRepository;
import com.azawisza.highscore.game.score.UserScore;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.azawisza.highscore.game.GameConsts.OK200;

/**
 * Created by azawisza on 16.10.2016.
 */
public class HighScoreList implements Service<Integer> {

    public static final String USER_SCORE_SEPARATOR = "=";
    public static final String DELIMITER = ",";
    private final ScoreRepository scoreRepository;

    public HighScoreList(ScoreRepository scoreRepository) {
        this.scoreRepository = scoreRepository;
    }

    @Override
    public ServiceResult service(Integer levelId) {
        List<UserScore> levelHighScores = scoreRepository.getHighScoreOfLevel(levelId);
        String formattedLevelIdScores = levelHighScores.stream()
                .map(formatUserScore())
                .collect(Collectors.joining(DELIMITER));
        return new ServiceResult(OK200, formattedLevelIdScores);
    }

    private Function<UserScore, String> formatUserScore() {
        return userScore -> userScore.getUserId() + USER_SCORE_SEPARATOR + userScore.getScore();
    }
}
