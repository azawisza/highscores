package com.azawisza.highscore.common;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by azawisza on 18.10.2016.
 */
public class ServerConfigurationBuilder {

    public GameServerConfiguration buildConfigurationFromArguments(String[] args) {
        GameServerConfiguration conf = new GameServerConfiguration()
                .withPort(8091)
                .withCorePoolSize(1)
                .withMaximumPoolSize(15)
                .withKeepAliveTime(60L)
                .withKeepAliveUnit(TimeUnit.SECONDS)
                .withMaxWaitingRq(10);
        Map<String, String> arguments = parseArguments(args);
        if (arguments.containsKey("port")) {
            conf.withPort(parseIntArg(arguments, "port"));
        }
        if (arguments.containsKey("corePoolSize")) {
            conf.withCorePoolSize(parseIntArg(arguments, "corePoolSize"));
        }
        if (arguments.containsKey("maximumPoolSize")) {
            conf.withMaximumPoolSize(parseIntArg(arguments, "maximumPoolSize"));
        }
        if (arguments.containsKey("maxWaitingRq")) {
            conf.withMaxWaitingRq(parseIntArg(arguments, "maxWaitingRq"));
        }
        if (arguments.containsKey("keepAliveTimeSec")) {
            conf.withKeepAliveTime(parseIntArg(arguments, "keepAliveTimeSec"));
        }
        if (arguments.containsKey("loggingEnabled")) {
            conf.withLoggingEnabled(Boolean.parseBoolean(arguments.get("loggingEnabled")));
        }
        return conf;
    }

    private int parseIntArg(Map<String, String> arguments, String port) {
        return Integer.parseInt(arguments.get(port));
    }

    private Map<String, String> parseArguments(String[] args) {
        List<String> strings = Arrays.asList(args);
        Map<String, String> result = new HashMap<>();
        for (String option : strings) {
            if (option.contains("=")) {
                String[] split = option.split("=");
                if (split.length == 2) {
                    result.put(split[0], split[1]);
                }
            }
        }
        return result;
    }


}
