package com.azawisza.highscore.common.service;

import java.util.function.Predicate;

/**
 * Created by azawisza on 16.10.2016.
 */
public class DeclarableService {

    private Predicate<ServiceRequest> serviceMethodCondition;
    private Service method;
    private ServiceRequestConverter converter;

    public DeclarableService(Predicate<ServiceRequest> condition) {
        this.serviceMethodCondition = condition;
    }

    public boolean matches(ServiceRequest request) {
        boolean test = serviceMethodCondition.test(request);
        return test;
    }

    public Service get() {
        return method;
    }

    public ServiceRequestConverter getConverter() {
        return converter;
    }

    public static DeclarableService when(Predicate<ServiceRequest> condition) {
        return new DeclarableService(condition);
    }

    public <RQ> DeclarableService thenPassTo(Service<RQ> method) {
        this.method = method;
        return this;
    }

    public <RQ> DeclarableService convertRequestWith(ServiceRequestConverter<RQ> converter) {
        this.converter = converter;
        return this;
    }
}
