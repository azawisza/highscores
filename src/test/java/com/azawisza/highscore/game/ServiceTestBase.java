package com.azawisza.highscore.game;

import com.azawisza.highscore.common.service.ServiceResult;
import com.azawisza.highscore.game.repositories.ScoreRepository;
import com.azawisza.highscore.game.repositories.UserSessionRepository;
import com.azawisza.highscore.game.highscore.HighScoreList;
import com.azawisza.highscore.game.login.UserLoginService;
import com.azawisza.highscore.game.score.ScoreRequest;
import com.azawisza.highscore.game.score.ScoreService;
import org.junit.After;
import org.junit.Before;

import java.util.concurrent.CountDownLatch;

/**
 * Created by azawisza on 18.10.2016.
 */
public abstract class ServiceTestBase {

    protected GameConfiguration configuration = new GameConfiguration()
            .withSessionLength(GameConsts.USER_SESSION_LENGTH)
            .withThreshold(GameConsts.THRESHOLD)
            .withMaxLevelResults(GameConsts.LEVEL_MAX_SCORES_COUNT);
    protected UserSessionRepository userSessionRepository = new UserSessionRepository(configuration);
    protected ScoreRepository scoreRepository = new ScoreRepository(configuration);
    protected UserLoginService userLoginService = new UserLoginService(userSessionRepository, configuration);
    protected HighScoreList scoreListService = new HighScoreList(scoreRepository);
    protected ScoreService scoreService = new ScoreService(userSessionRepository, scoreRepository);

    @Before
    public void setUp() {
        userSessionRepository = new UserSessionRepository(configuration);
        scoreRepository = new ScoreRepository(configuration);
        userLoginService = new UserLoginService(userSessionRepository, configuration);
        scoreListService = new HighScoreList(scoreRepository);
        scoreService = new ScoreService(userSessionRepository, scoreRepository);
    }

    @After
    public void tearDown() {
        configuration.withSessionLength(GameConsts.USER_SESSION_LENGTH);
    }

    protected ServiceResult callWithScore(int levelId, String sessionId, int score) {
        return scoreService.service(new ScoreRequest()
                .withLevelId(levelId)
                .withScore(score)
                .withSessionId(sessionId));
    }

    protected Runnable sessionInvalidationCheck(String oldSession, CountDownLatch latch) {
        return () -> {
            ServiceResult service = userLoginService.service(3213);
            while (service.getResult().equals(oldSession)) {
                service = userLoginService.service(3213);
                try {
                    Thread.currentThread().sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            latch.countDown();
        };
    }

}
