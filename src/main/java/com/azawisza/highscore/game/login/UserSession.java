package com.azawisza.highscore.game.login;

/**
 * Created by azawisza on 18.10.2016.
 */
public class UserSession {

    private Long expiry;
    private String sessionId;


    public UserSession(Long expiry, String sessionId) {
        this.expiry = expiry;
        this.sessionId = sessionId;
    }

    public Long getExpiry() {
        return expiry;
    }

    public String getSessionId() {
        return sessionId;
    }

}
