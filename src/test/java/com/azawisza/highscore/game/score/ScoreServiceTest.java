package com.azawisza.highscore.game.score;

import com.azawisza.highscore.common.service.ServiceResult;
import com.azawisza.highscore.game.ServiceTestBase;
import com.azawisza.highscore.game.score.UserScore;
import org.fest.assertions.Assertions;
import org.junit.Test;

import java.util.List;

/**
 * Created by azawisza on 18.10.2016.
 */
public class ScoreServiceTest extends ServiceTestBase {

    @Test
    public void shouldAddScore() {
        //given
        int levelId = 21;
        String sessionId = userLoginService.service(1233).getResult();
        ServiceResult service = callWithScore(levelId, sessionId, 12);
        //when
        List<UserScore> highScoreOfLevel = scoreRepository.getHighScoreOfLevel(levelId);
        //then
        Assertions.assertThat(service.getResult()).isEqualTo("");
        Assertions.assertThat(service.getCode()).isEqualTo(200);
        Assertions.assertThat(highScoreOfLevel).contains(new UserScore(1233, 12));
    }

    @Test
    public void shouldStoreHigherScoreOfUser() {
        //given
        int levelId = 21;
        String sessionId = userLoginService.service(1233).getResult();
        callWithScore(levelId, sessionId, 12);
        callWithScore(levelId, sessionId, 25);
        //when
        List<UserScore> highScoreOfLevel = scoreRepository.getHighScoreOfLevel(levelId);
        //then
        Assertions.assertThat(highScoreOfLevel).contains(new UserScore(1233, 25));
    }

    @Test
    public void shouldNotStoreLowerScoreOfUser() {
        //given
        int levelId = 21;
        String sessionId = userLoginService.service(1233).getResult();
        callWithScore(levelId, sessionId, 12);
        callWithScore(levelId, sessionId, 2);
        //when
        List<UserScore> highScoreOfLevel = scoreRepository.getHighScoreOfLevel(levelId);
        //then
        Assertions.assertThat(highScoreOfLevel).contains(new UserScore(1233, 12));
    }

    @Test
    public void shouldSortScoresAscending() {
        //given
        int levelId = 21;
        int levelId2 = 222;

        setupWith3EntriesLevelOf(levelId);
        setupWith3EntriesLevelOf(levelId2);
        //when
        List<UserScore> highScoreOfLevel = scoreRepository.getHighScoreOfLevel(levelId);
        List<UserScore> highScoreOfLevel2 = scoreRepository.getHighScoreOfLevel(levelId2);
        //then
        Assertions.assertThat(highScoreOfLevel.get(0).getScore()).isEqualTo(1322);
        Assertions.assertThat(highScoreOfLevel.get(1).getScore()).isEqualTo(112);
        Assertions.assertThat(highScoreOfLevel.get(2).getScore()).isEqualTo(68);
        Assertions.assertThat(highScoreOfLevel2.get(0).getScore()).isEqualTo(1322);
        Assertions.assertThat(highScoreOfLevel2.get(1).getScore()).isEqualTo(112);
        Assertions.assertThat(highScoreOfLevel2.get(2).getScore()).isEqualTo(68);
    }

    @Test
    public void shouldFilterSurplusAndLowestScore() {
        //given
        int levelId = 21;
        configuration.setMaxLevelResults(3);
        setupWith3EntriesLevelOf(levelId);
        //when
        String sessionId4 = userLoginService.service(14).getResult();
        callWithScore(levelId, sessionId4, 68);
        List<UserScore> highScoreOfLevel = scoreRepository.getHighScoreOfLevel(levelId);
        //then
        Assertions.assertThat(highScoreOfLevel.size()).isEqualTo(3);
        Assertions.assertThat(highScoreOfLevel.get(0).getScore()).isEqualTo(1322);
        Assertions.assertThat(highScoreOfLevel.get(1).getScore()).isEqualTo(112);
        Assertions.assertThat(highScoreOfLevel.get(2).getScore()).isEqualTo(68);
    }



    private void setupWith3EntriesLevelOf(int levelId) {
        String sessionId1 = userLoginService.service(11).getResult();
        String sessionId2 = userLoginService.service(12).getResult();
        String sessionId3 = userLoginService.service(13).getResult();
        callWithScore(levelId, sessionId1, 112);
        callWithScore(levelId, sessionId2, 1322);
        callWithScore(levelId, sessionId3, 68);
    }

}