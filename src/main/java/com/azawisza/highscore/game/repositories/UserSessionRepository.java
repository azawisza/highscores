package com.azawisza.highscore.game.repositories;

import com.azawisza.highscore.game.GameConfiguration;
import com.azawisza.highscore.game.login.UserSession;

import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiFunction;


/**
 * Created by azawisza on 16.10.2016.
 */
public class UserSessionRepository {

    private final ConcurrentHashMap<Integer, UserSession> users = new ConcurrentHashMap<>();
    private final GameConfiguration conf;

    public UserSessionRepository(GameConfiguration conf) {
        this.conf = conf;
    }

    public String upsertAndLoginUser(int userid, UserSession possibleNewSession,
                                     BiFunction<UserSession, UserSession, UserSession> invalidateSession) {
        UserSession currentSessionOfUser = users.merge(userid, possibleNewSession,
                invalidateSession);
        return currentSessionOfUser.getSessionId();
    }

    public Integer findUserOfSessionId(String sessionId) {
        return users.search(conf.getThreshold(),
                firstMatchOrNull(sessionId));
    }


    private BiFunction<Integer, UserSession, Integer> firstMatchOrNull(String search) {
        return (key, value) -> {
            String sessionId = value.getSessionId();
            return sessionId.equals(search) ? key : null;
        };
    }


}
