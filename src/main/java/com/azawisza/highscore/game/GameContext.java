package com.azawisza.highscore.game;


import com.azawisza.highscore.game.highscore.HighScoreList;
import com.azawisza.highscore.game.highscore.HighScoreRequestConverter;
import com.azawisza.highscore.game.login.LoginRequestConverter;
import com.azawisza.highscore.game.login.UserLoginService;
import com.azawisza.highscore.game.repositories.ScoreRepository;
import com.azawisza.highscore.game.repositories.UserSessionRepository;
import com.azawisza.highscore.game.score.ScoreRequestConverter;
import com.azawisza.highscore.game.score.ScoreService;
import com.azawisza.highscore.common.service.DeclarableService;
import com.azawisza.highscore.common.service.Service;
import com.azawisza.highscore.common.service.ServiceContext;
import com.azawisza.highscore.common.service.ServiceRequest;

import java.util.List;
import java.util.function.Predicate;

import static com.azawisza.highscore.common.service.DeclarableService.when;
import static com.azawisza.highscore.common.service.ServiceMethodPredicates.*;
import static java.util.Arrays.asList;


/**
 * Created by azawisza on 16.10.2016.
 */
public class GameContext implements ServiceContext {

    private final List<DeclarableService> serviceMethods;

    private final GameConfiguration configuration = new GameConfiguration()
            .withSessionLength(GameConsts.USER_SESSION_LENGTH)
            .withThreshold(GameConsts.THRESHOLD)
            .withMaxLevelResults(GameConsts.LEVEL_MAX_SCORES_COUNT);
    private final UserSessionRepository userSessionRepository = new UserSessionRepository(configuration);
    private final ScoreRepository scoreRepository = new ScoreRepository(configuration);
    private final Service userLoginService = new UserLoginService(userSessionRepository,configuration);
    private final Service scoreListService = new HighScoreList(scoreRepository);
    private final Service scoreService = new ScoreService(userSessionRepository, scoreRepository);

    public GameContext() {
        serviceMethods = asList(
                when(methodGet.and(pathIsLogin))
                        .convertRequestWith(new LoginRequestConverter())
                        .thenPassTo(userLoginService),
                when(methodGet.and(pathIsHighscorelist))
                        .convertRequestWith(new HighScoreRequestConverter())
                        .thenPassTo(scoreListService),
                when(methodPost.and(pathIsScore).and(hasQuery))
                        .convertRequestWith(new ScoreRequestConverter())
                        .thenPassTo(scoreService)
        );
    }

    @Override
    public boolean supports(ServiceRequest request) {
        return pathOfThree.and(methodGet.or(methodPost)).test(request);
    }

    @Override
    public List<DeclarableService> methods() {
        return serviceMethods;
    }

    private static final Predicate<ServiceRequest> pathIsLogin = (path) -> secondPathElementIs.test(path, "login");
    private static final Predicate<ServiceRequest> pathIsHighscorelist = (path) -> secondPathElementIs.test(path, "highscorelist");
    private static final Predicate<ServiceRequest> pathIsScore = (path) -> secondPathElementIs.test(path, "score");

}
