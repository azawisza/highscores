package com.azawisza.highscore.common;

import com.azawisza.highscore.game.GameContext;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static com.azawisza.highscore.common.server.DefaultHandler.handle;
import static com.azawisza.highscore.common.server.ServerUtils.send;

/**
 * Created by azawisza on 15.10.2016.
 */
public class GameServerContext implements Iterable<Map.Entry<String, HttpHandler>> {

    private final Map<String, HttpHandler> map = new HashMap<>();


    public GameServerContext() {
        map.put("/test", (HttpExchange arg) -> send(arg, "This is test", 200));
        map.put("/", handle(new GameContext()));
    }


    @Override
    public Iterator<Map.Entry<String, HttpHandler>> iterator() {
        return map.entrySet().iterator();
    }
}
