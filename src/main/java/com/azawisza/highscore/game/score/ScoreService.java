package com.azawisza.highscore.game.score;

import com.azawisza.highscore.common.service.Service;
import com.azawisza.highscore.common.service.ServiceResult;
import com.azawisza.highscore.game.repositories.ScoreRepository;
import com.azawisza.highscore.game.repositories.UserSessionRepository;

import static com.azawisza.highscore.game.GameConsts.NotFound404;
import static com.azawisza.highscore.game.GameConsts.OK200;

/**
 * Created by azawisza on 16.10.2016.
 */
public class ScoreService implements Service<ScoreRequest> {

    private final UserSessionRepository sessionsRepository;
    private final ScoreRepository scoreRepository;

    public ScoreService(UserSessionRepository sessionsRepository, ScoreRepository scoreRepository) {
        this.sessionsRepository = sessionsRepository;
        this.scoreRepository = scoreRepository;
    }

    @Override
    public ServiceResult service(ScoreRequest request) {
        String sessionId = request.getSessionId();
        Integer userOfSessionId = sessionsRepository.findUserOfSessionId(sessionId);
        if (userOfSessionId == null) {
            return new ServiceResult(NotFound404, "user not found " + sessionId);
        } else {
            scoreRepository.addUserScoreToLevel(request.getLevelId(), userOfSessionId, request.getScore());
        }
        return new ServiceResult(OK200, "");
    }

}
