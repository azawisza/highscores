package com.azawisza.highscore.common;

/**
 * Created by azawisza on 26.10.2016.
 */
public class HttpCodes {
    public static final int OK200 = 200;
    public static final int BadRequest400 = 400;
    public static final int ServerError500 = 500;
    public static final int NotFound = 404;
}
