package com.azawisza.highscore.common.server;

import com.sun.net.httpserver.HttpExchange;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by azawisza on 16.10.2016.
 */
public class ServerUtils {

    public static void send(HttpExchange arg, String response, int code) {
        try (OutputStream output = arg.getResponseBody()) {
            arg.sendResponseHeaders(code, response.length());
            output.write(response.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
