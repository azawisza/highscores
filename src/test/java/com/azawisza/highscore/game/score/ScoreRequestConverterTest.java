package com.azawisza.highscore.game.score;

import com.azawisza.highscore.common.service.ServiceRequest;
import com.azawisza.highscore.game.GameException;
import org.fest.assertions.Assertions;
import org.junit.Test;

import static java.util.Arrays.asList;

/**
 * Created by azawisza on 17.10.2016.
 */
public class ScoreRequestConverterTest {

    @Test
    public void shouldConvert() {
        //given
        ScoreRequestConverter converter = new ScoreRequestConverter();
        //when
        ScoreRequest request = converter.convert(
                new ServiceRequest("POST", asList("", "112", "score"), "100", asList("sessionkey=12321s2121")));
        //then
        Assertions.assertThat(request.getLevelId()).isEqualTo(112);
        Assertions.assertThat(request.getScore()).isEqualTo(100);
        Assertions.assertThat(request.getSessionId()).isEqualTo("12321s2121");
    }

    @Test(expected = GameException.class)
    public void shouldExceptionWhenNoSessionKey() {
        //given
        ScoreRequestConverter converter = new ScoreRequestConverter();
        //when
        ScoreRequest request = converter.convert(
                new ServiceRequest("POST", asList("", "112", "score"), "100", asList("sessio=12321s2121")));
        //then
    }

}