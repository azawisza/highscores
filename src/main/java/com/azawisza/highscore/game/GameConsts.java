package com.azawisza.highscore.game;

/**
 * Created by azawisza on 18.10.2016.
 */
public class GameConsts {

    public static final int OK200 = 200;
    public static final int NotFound404 = 404;
    public static final int BadRequest400 = 400;
    public static final int ServerError500 = 500;
    public static final int USER_SESSION_LENGTH = 10 * 60 * 1000;
    public static final int LEVEL_MAX_SCORES_COUNT = 15;
    public static final long THRESHOLD = 100;
}
